#coding: utf-8
from fabric.api import env, run, cd
from fabric.decorators import task


HOST_USER = {
    'bit-dev': 'bit-dev',
    'dobby': 'dobby',
}

DJANGO_PROJECT_NAME = {
    'bit-dev': 'bit-t',
    'dobby': 'dobby',
}

env.roledefs = {
    'dev': ['doffee@37.139.12.74'],
    'production': ['production@37.139.12.74'],
    #'bit-dev': [HOST_USER['bit-dev']+'@95.85.23.161'],
    'dobby': ['dobby@37.139.12.74'],
}


HOME = '~/'
VENV = HOME + 'venv/'
PYTHON = VENV + 'bin/python2'
PIP = VENV + 'bin/pip'
MANAGE = PYTHON + ' manage.py'
MIGRATE = MANAGE + ' migrate'
UWSGI_TOUCH = HOME + '.uwsgi_reload'
USER = HOST_USER[env['roles'][0]]
PROJECT_PATH = HOME + 'django/'
DJANGO_PROJECT_PATH = PROJECT_PATH + DJANGO_PROJECT_NAME[env['roles'][0]] + '/'


def git_update():
    run('git pull')


@task
def install_req():
    with cd(PROJECT_PATH):
        run('%s install -r requirements.txt' % PIP)


@task
def migrate(migrate_arg=''):
    with cd(DJANGO_PROJECT_PATH):
        run('%s %s' %
            (MIGRATE, migrate_arg))


@task
def restart_uwsgi():
    run('touch %s' % UWSGI_TOUCH)
    print('All alright.')


# @task
# def flush_memcached():
#     with cd(DJANGO_PROJECT_PATH):
#         run('%s FlushMemcached' % MANAGE)
#     print('All alright.')


@task
def update(syncdb_arg='', **kwargs):
    with cd(PROJECT_PATH):
        git_update()
    with cd(DJANGO_PROJECT_PATH):
        run('%s syncdb %s' %
            (MANAGE, syncdb_arg))
    migrate(**kwargs)
    restart_uwsgi()
    # flush_memcached()


def flush_db():
    with cd(DJANGO_PROJECT_PATH):
        run('%s flush' % MANAGE)


@task
def show_logs(lines=30):
    with cd(HOME + 'logs/'):
        run('tail -n %s django-dev.log' % lines)


@task
def test():
    print(DJANGO_PROJECT_PATH)
    print(USER)