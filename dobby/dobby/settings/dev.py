import os

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


DEBUG = True
DEBUG_TOOLBAR_ENABLED = False
SETTINGS_TYPE = 'dev'
TEMPLATE_DEBUG = DEBUG

MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = (os.path.join(PROJECT_DIR, 'static'),)
STATIC_ROOT = ''
STATIC_URL = STATICFILES_URL = '/static/'

COMPRESS_ENABLED = False
COMPRESS_OUTPUT_DIR = 'CACHE'
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',
                        'compressor.filters.cssmin.CSSMinFilter']

ADMIN_MEDIA_PREFIX = '/static/admin/'
FIXTURE_DIRS = (PROJECT_DIR + '/fixtures/',)

DEBUG_APPS = ('rosetta', )

if DEBUG_TOOLBAR_ENABLED:
    DEBUG_TOOLBAR_CONFIG = {
        "INTERCEPT_REDIRECTS": False
    }
    DEBUG_APPS += ('debug_toolbar',)
    DEBUG_MIDDLEWARE_CLASSES = (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    INTERNAL_IPS = ('127.0.0.1',)

try:
    from base import *
except ImportError:
    print('NOT base.py')

try:
    MIDDLEWARE_CLASSES += DEBUG_MIDDLEWARE_CLASSES
except NameError:
    pass

try:
    INSTALLED_APPS += DEBUG_APPS
except NameError:
    pass

# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#         'LOCATION': '127.0.0.1:11211',
#     }
# }

try:
    from environment import *
except ImportError:
    print('NOT environment.py')

