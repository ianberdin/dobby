# -*- coding: utf-8 -*-
import os

from secret import Secret
from decimal import Decimal

secret = Secret()

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

ADMINS = (
    ('happierall', 'happierall@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': secret.db_engine,
        'NAME': secret.db_name,
        'USER': secret.db_username,
        'PASSWORD': secret.db_password,
        'HOST': secret.db_host,
        'PORT': secret.db_port,
    }
}

LOCALE_PATHS = (
    os.path.join(PROJECT_DIR, 'locale'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder'
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.debug',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

if not secret.use_local_mail:
    EMAIL_BACKEND = secret.email_backend
    DEFAULT_FROM_EMAIL = secret.default_from_email
    EMAIL_USE_TLS = secret.email_use_tls
    EMAIL_HOST = secret.email_host
    EMAIL_PORT = secret.email_port
    EMAIL_HOST_PASSWORD = secret.email_host_password
else:
    DEFAULT_FROM_EMAIL = 'root@localhost'
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    EMAIL_HOST = '127.0.0.1:8000'
    EMAIL_PORT = '1025'

EMAIL_HOST_USER = secret.email_host_user

ROOT_URLCONF = 'dobby.urls'

WSGI_APPLICATION = 'dobby.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)

AUTHENTICATION_BACKENDS = (
    # 'users.authBackend.AuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ACCOUNT_ACTIVATION_DAYS = 2

SECRET_KEY = secret.SECRET_KEY

LOGIN_URL = '/login'
LOGIN_ERROR_URL = '/login/error/'

AUTH_USER_MODEL = 'users.User'

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = (
    'compressor',
    'south',
    'sanitizer',
    'grappelli',
    'tastypie',
    'django_extensions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
)

MY_APPS = (
    'apps.core',
    'apps.users',
)

INSTALLED_APPS += MY_APPS

TIME_ZONE = 'Asia/Yekaterinburg'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

ugettext = lambda s: s

LANGUAGE_CODE = 'ru'

LANGUAGES = (
    ('ru', ugettext('Russian')),
)

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#         'LOCATION': 'default-cache'
#     }
# }

API_KEY = 'goodKeyForNoNotGoodkey'
