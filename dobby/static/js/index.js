jQuery(document).ready(function () {
    // info
    var distance = $('#distance').children('.value');
    var time = $('#time').children('.value');
    var cost = $('#cost').children('.value');

    // change city from
    $('#from').change(function () {
        var city = $(this).find(":selected");
        var point = $('#point_from');
        var from = $(this).val();
        var to = $('#to').val();

        if (from !== '-1' && to !== '-1') {
            point.css('left', city.data('x'));
            point.css('top', city.data('y'));
            point.find('.label').text(city.text());

            if (from.length && to.length) {
                if (from !== to) {
                    update_data(from, to);
                } else {
                    jQuery('#message').html('Пожалуйста, выберите другой город.');
                }
            }
        } else {
            jQuery('#message').html('');
        }
    });

    $('#to').change(function () {
        var city = $(this).find(":selected");
        var point = $('#point_to');
        var to = $(this).val();
        var from = $('#from').val();
        if (from !== '-1' && to !== '-1') {

            point.css('left', city.data('x'));
            point.css('top', city.data('y'));
            point.find('.label').text(city.text());

            if (from.length && to.length) {
                if (from !== to) {
                    update_data(from, to);
                } else {
                    jQuery('#message').html('Пожалуйста, выберите другой город.');
                }
            }
        } else {
            jQuery('#message').html('');
        }
    });

    function update_data(from, to) {
        var data = [];
        data.push({name: 'csrfmiddlewaretoken', value: csrf_token});
        data.push({ name: 'from', value: from });
        data.push({ name: 'to', value: to });

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (!data.error) {
                    jQuery('#message').html('');
                    distance.text(data.distance);
                    time.text(data.time);
                    cost.text(data.cost);
                } else {
                    jQuery('#message').html(data.error);
                }
            },
            error: function (err) {
                console.log(err);
            },
        });
    }

});