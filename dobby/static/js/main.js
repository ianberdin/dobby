jQuery(document).ready(function () {
// Events when click on pseudo elements(triangle)
    $('.cselect').click(function (e) {
        var $select = $(this).children('select');
        if (!$(e.target).is($select)) {
            openSelect($select);
        }
    });

    function openSelect(elem) {
        if (document.createEvent) {
            var e = document.createEvent("MouseEvents");
            e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            elem[0].dispatchEvent(e);
        } else if (element.fireEvent) {
            elem[0].fireEvent("onmousedown");
        }
    }
});