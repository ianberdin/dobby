from apps.users.models import User


class AuthBackend(object):
    def authenticate(self, email=None, password=None):
        print 1
        try:
            print 2
            user = User.objects.get(email=email)

            print 3
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

#
# class AuthBackend(object):
#     def authenticate(self, email=None, password=None):
#         user = None
#         try:
#             user_tmp = User.objects.get(email=email)
#         except User.DoesNotExist:
#             return None
#         else:
#             if user_tmp.check_password(password):
#                 user = user_tmp
#                 user.backend = 'django.contrib.auth.backends.ModelBackend'
#                 user.is_social = False
#                 user.save()
#         return user
