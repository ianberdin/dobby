# coding: utf-8
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('id', 'password')