# -*- coding: utf-8 -*-
import urllib
import json
from decimal import Decimal

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, RequestContext
from django.core.urlresolvers import reverse
from django.db.models import Q, F
from django.contrib import messages
from django.contrib.auth.views import logout as auth_logout
from django.contrib.auth import login as logon
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required

from apps.core.utils import get_object_or_none

from .models import User


def login(request):
    data = {}
    NEXT = request.GET.get('next')
    if NEXT:
        data['next'] = NEXT
    if request.method == "POST":
        next_url = request.POST.get('next')

        if request.user.is_authenticated():
            message = 'You are logged'
        else:
            email = request.POST.get('email')
            password = request.POST.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                if user.is_active:
                    logon(request, user)
                    if next_url:
                        return redirect(next_url)
                    return redirect('/')
                else:
                    message = 'Your account is not activated'
            else:
                if next_url:
                    data['next'] = next_url
                message = 'Please check the correctness of the data'
        try:
            data['message'] = message
        except UnboundLocalError:
            pass
    return render(request, 'login.html', data)


def logout_action(request, next_page='/'):
    language = None
    if hasattr(request, 'session'):
        if 'django_language' in request.session:
            language = request.session['django_language']
    response = auth_logout(request, next_page)
    if language:
        request.session['django_language'] = language
    return response