from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns

from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'is_admin', 'is_active')


admin.site.register(User, UserAdmin)