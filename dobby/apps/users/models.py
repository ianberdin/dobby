# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import re
from decimal import Decimal

from django.conf import settings
from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db.models.signals import post_save

from django.dispatch import receiver

try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now


class UserManager(BaseUserManager):
    def create_user(self, email=None, password=None, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        user = self.create_user(
            email,
            password,
            **extra_fields
        )
        user.is_admin = True
        user.is_superuser = True

        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    objects = UserManager()

    email = models.EmailField(max_length=75, unique=True, db_index=True, verbose_name='Email')
    username = models.CharField('username', max_length=70, blank=True, null=True)
    first_name = models.CharField('First name', max_length=50, blank=True, null=True)
    last_name = models.CharField('Last name', max_length=50, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name

    def get_username(self):
        if self.username:
            return self.username
        return 'user%s' % self.id

    def __unicode__(self):
        return self.email

    def has_perms(self, perm, obj=None):
        return True

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def is_authenticated(self):
        return True

    def email_user(self, subject, message, from_email):
        send_mail(subject, message, from_email, [self.email])

    def user_expired(self):
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.user.created + expiration_date <= datetime_now()