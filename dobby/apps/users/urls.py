# coding: utf-8
from django.conf.urls import patterns, url


urlpatterns = patterns('apps.users.views',
                       url(r'^login$', 'login', name='login'),
                       url(r'^logout$', 'logout_action', name='logout'),
)

