# coding: utf-8
from django.conf.urls import patterns, url, include
from .api import CityResource, TrackResource, CompanyResource, OrderResource, UserResource, OrderPutResource
from tastypie.api import Api


v1_api = Api(api_name='v1')
v1_api.register(CityResource())
v1_api.register(TrackResource())
v1_api.register(CompanyResource())
v1_api.register(OrderResource())
v1_api.register(OrderPutResource())
v1_api.register(UserResource())

urlpatterns = patterns('apps.core.views',
                       url(r'^$', 'home', name='home'),
                       url(r'^api/', include(v1_api.urls)),
                       )
