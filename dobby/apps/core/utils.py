# -*- coding: utf-8 -*-
import os
import hashlib
import contextlib
import errno
import simplejson as json
import time
import base64
import decimal
from django.conf import settings


def get_object_or_none(klass, *args, **kwargs):
    try:
        return klass._default_manager.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


def ccyformat(value):
    if not value:
        return 0
    if not (isinstance(value, float) or isinstance(value, decimal.Decimal)):
        return str(value).rstrip('0').rstrip('.')
    return "%.2f" % value


class DecimalJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalJSONEncoder, self).default(o)


def rupluralize_num(value, arg="дурак,дурака,дураков"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return '%s %s' % (value, args[0])
    elif (a >= 2) and (a <= 4) and (b < 10) or (b >= 20):
        return '%s %s' % (value, args[1])
    else:
        return '%s %s' % (value, args[2])