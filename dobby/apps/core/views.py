# -*- coding: utf-8 -*-
import json
import qrcode
import StringIO
from decimal import Decimal, InvalidOperation

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, Http404
from django.contrib import messages
from django.utils.translation import gettext as _, get_language
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings

from .models import City, Track, Company
from .utils import get_object_or_none, rupluralize_num


def home(request):
    rp = request.POST
    data = {}

    if rp:
        fr = rp.get('from', None)
        to = rp.get('to', None)
        if fr and to:
            track = get_object_or_none(Track,
                                       city_from__id=int(fr),
                                       city_to__id=int(to))
            if track:
                data['distance'] = u'%s км' % track.distance
                data['time'] = rupluralize_num(track.time, arg="час,часа,часов")
                data['cost'] = '%s руб' % track.cost
                # TODO add companies

                return HttpResponse(json.dumps(data))
            data['error'] = u'Извините, по данному маршруту пока не обслуживаем'
            return HttpResponse(json.dumps(data))

    data['cities'] = City.objects.all()
    return render(request, 'index.html', data)
