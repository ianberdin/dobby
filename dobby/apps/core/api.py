import requests
from tastypie.resources import ModelResource
from tastypie import fields
from .api_authentication import CustomAuthentication, CustomAuthorization, SessionAuthentication
from tastypie.authorization import DjangoAuthorization, Authorization
from .models import City, Track, Company, Order

from django.contrib.auth import get_user_model
from django.conf import settings
from django.db import IntegrityError
from tastypie import http
from tastypie.exceptions import ImmediateHttpResponse
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash

User = settings.AUTH_USER_MODEL
User = get_user_model()


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        fields = ['first_name', 'last_name', 'email']
        allowed_methods = ['get', 'post']
        resource_name = 'user'
        authorization = DjangoAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
        ]

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))

        email = data.get('email', '')
        password = data.get('password', '')

        user = authenticate(email=email, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return self.create_response(request, {
                    'success': True
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'reason': 'disabled',
                }, HttpForbidden)
        else:
            return self.create_response(request, {
                'success': False,
                'reason': 'incorrect',
            }, HttpUnauthorized)

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, {'success': True})
        else:
            return self.create_response(request, {'success': False}, HttpUnauthorized)


class CompanyResource(ModelResource):
    class Meta:
        queryset = Company.objects.all()
        resource_name = 'company'
        allowed_methods = ['get']
        authentication = CustomAuthentication()


class CityResource(ModelResource):
    class Meta:
        queryset = City.objects.all()
        resource_name = 'city'
        allowed_methods = ['get']
        authentication = CustomAuthentication()


class TrackResource(ModelResource):
    city_from = fields.ForeignKey(CityResource, attribute='city_from', full=True)
    city_to = fields.ForeignKey(CityResource, attribute='city_to', full=True)
    companies = fields.ToManyField(CompanyResource, attribute='companies', null=True, blank=True, full=True)

    class Meta:
        queryset = Track.objects.all()
        resource_name = 'track'
        allowed_methods = ['get']
        authentication = CustomAuthentication()


class OrderResource(ModelResource):
    track = fields.ForeignKey(TrackResource, attribute='track', full=True)

    class Meta:
        queryset = Order.objects.all()
        resource_name = 'order'
        allowed_methods = ['get', 'put', 'post']
        # authentication = CustomAuthentication()
        # authorization = DjangoAuthorization()
        authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        try:
            bundle = super(OrderResource, self).obj_create(bundle, **kwargs)
            bundle.obj.save()
        except IntegrityError:
            raise Exception('The order already exists')
        return bundle

    def obj_get(self, request=None, **kwargs):
        try:
            return super(OrderResource, self).obj_get(request=request, **kwargs)
        except ObjectDoesNotExist:
            # only branch if kwargs were supplied, which indicates filtering
            # should be applied. If there's no search parameters (not even a pk),
            # then testing for existence is pointless and can cause incorrect
            # returning of http.HttpUnauthorized() (sometimes this method is
            # called on a related resource without any kwargs).
            if kwargs:
                exists = self.get_object_list(request).filter(**kwargs).exists()
                # If the object exists but this particular user doesn't have access,
                # then we should let users know they're unauthorized instead of
                # saying the object doesn't exist. Also, if TastyPie thinks the
                # object doesn't exist, sometimes it'll try to recreate it (such
                # as during a PUT request), resulting in a DB integrity error
                # from trying to insert an existing primary key, and this snippet
                # prevents that.
                if exists:
                    raise ImmediateHttpResponse(response=http.HttpUnauthorized())
            raise


class OrderPutResource(ModelResource):
    track = fields.ForeignKey(TrackResource, attribute='track', full=True)

    class Meta:
        queryset = Order.objects.all()
        resource_name = 'order_put'
        allowed_methods = ['put']
        # authentication = CustomAuthentication()
        # authorization = DjangoAuthorization()
        authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        try:
            bundle = super(OrderPutResource, self).obj_create(bundle, **kwargs)
            bundle.obj.save()
        except IntegrityError:
            raise Exception('The order already exists')
        return bundle
