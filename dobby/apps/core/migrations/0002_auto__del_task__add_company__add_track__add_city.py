# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Task'
        db.delete_table(u'core_task')

        # Adding model 'Company'
        db.create_table(u'core_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Company'])

        # Adding model 'Track'
        db.create_table(u'core_track', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city_from', self.gf('django.db.models.fields.related.ForeignKey')(related_name='city_from', to=orm['core.City'])),
            ('city_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name='city_to', to=orm['core.City'])),
            ('distance', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('cost', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'core', ['Track'])

        # Adding M2M table for field companies on 'Track'
        m2m_table_name = db.shorten_name(u'core_track_companies')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('track', models.ForeignKey(orm[u'core.track'], null=False)),
            ('company', models.ForeignKey(orm[u'core.company'], null=False))
        ))
        db.create_unique(m2m_table_name, ['track_id', 'company_id'])

        # Adding model 'City'
        db.create_table(u'core_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('x', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('y', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'core', ['City'])


    def backwards(self, orm):
        # Adding model 'Task'
        db.create_table(u'core_task', (
            ('body', self.gf('django.db.models.fields.TextField')(max_length=1000)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal(u'core', ['Task'])

        # Deleting model 'Company'
        db.delete_table(u'core_company')

        # Deleting model 'Track'
        db.delete_table(u'core_track')

        # Removing M2M table for field companies on 'Track'
        db.delete_table(db.shorten_name(u'core_track_companies'))

        # Deleting model 'City'
        db.delete_table(u'core_city')


    models = {
        u'core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'x': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'y': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.track': {
            'Meta': {'object_name': 'Track'},
            'city_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_from'", 'to': u"orm['core.City']"}),
            'city_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_to'", 'to': u"orm['core.City']"}),
            'companies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['core.Company']", 'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'distance': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['core']