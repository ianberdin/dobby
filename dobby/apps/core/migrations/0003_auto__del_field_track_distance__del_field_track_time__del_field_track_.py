# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Track.distance'
        db.delete_column(u'core_track', 'distance')

        # Deleting field 'Track.time'
        db.delete_column(u'core_track', 'time')

        # Deleting field 'Track.cost'
        db.delete_column(u'core_track', 'cost')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Track.distance'
        raise RuntimeError("Cannot reverse this migration. 'Track.distance' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Track.distance'
        db.add_column(u'core_track', 'distance',
                      self.gf('django.db.models.fields.CharField')(max_length=30),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Track.time'
        raise RuntimeError("Cannot reverse this migration. 'Track.time' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Track.time'
        db.add_column(u'core_track', 'time',
                      self.gf('django.db.models.fields.CharField')(max_length=30),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Track.cost'
        raise RuntimeError("Cannot reverse this migration. 'Track.cost' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Track.cost'
        db.add_column(u'core_track', 'cost',
                      self.gf('django.db.models.fields.CharField')(max_length=30),
                      keep_default=False)


    models = {
        u'core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'x': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'y': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.track': {
            'Meta': {'object_name': 'Track'},
            'city_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_from'", 'to': u"orm['core.City']"}),
            'city_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_to'", 'to': u"orm['core.City']"}),
            'companies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['core.Company']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['core']