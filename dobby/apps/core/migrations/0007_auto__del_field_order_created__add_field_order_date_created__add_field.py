# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Order.created'
        db.delete_column(u'core_order', 'created')

        # Adding field 'Order.date_created'
        db.add_column(u'core_order', 'date_created',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 5, 7, 0, 0), auto_now_add=True, blank=True),
                      keep_default=False)

        # Adding field 'Order.date_updated'
        db.add_column(u'core_order', 'date_updated',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 5, 7, 0, 0), auto_now=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Order.created'
        raise RuntimeError("Cannot reverse this migration. 'Order.created' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Order.created'
        db.add_column(u'core_order', 'created',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True),
                      keep_default=False)

        # Deleting field 'Order.date_created'
        db.delete_column(u'core_order', 'date_created')

        # Deleting field 'Order.date_updated'
        db.delete_column(u'core_order', 'date_updated')


    models = {
        u'core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'x': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'y': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.order': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Order'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 5, 7, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 5, 7, 0, 0)', 'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'active'", 'max_length': '70'}),
            'track': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Track']"})
        },
        u'core.track': {
            'Meta': {'object_name': 'Track'},
            'city_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_from'", 'to': u"orm['core.City']"}),
            'city_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city_to'", 'to': u"orm['core.City']"}),
            'companies': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Company']", 'symmetrical': 'False'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['core']