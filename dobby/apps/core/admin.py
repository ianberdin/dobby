from django.contrib import admin

from .models import City, Track, Company, Order


class CityAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ['name', 'x', 'y']


admin.site.register(City, CityAdmin)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'address')
    search_fields = ['name', 'x', 'y']


admin.site.register(Company, CompanyAdmin)


class TrackAdmin(admin.ModelAdmin):
    list_display = ('get_id', 'get_track', 'distance', 'time', 'cost', 'get_company_name')
    search_fields = ['city_from', 'city_to']
    filter = ['city_from', 'city_to']


admin.site.register(Track, TrackAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('get_id', 'get_track', 'get_company_name', 'message', 'date_created', 'status')
    filter = ['track', 'status']

admin.site.register(Order, OrderAdmin)