from tastypie.authentication import Authentication
from django.conf import settings
from tastypie.authorization import Authorization


class CustomAuthentication(Authentication):
    def is_authenticated(self, request, **kwargs):
        if request.GET.get('key', None) == settings.API_KEY:
          return True
        return False

    # Optional but recommended
    def get_identifier(self, request):
        return request.user


class CustomAuthorization(Authorization):
    def is_authorized(self, request, object=None):
        r = request.GET or request.POST
        if request.user.check_password(r.get('password')):
            return True
        else:
            return False


class SessionAuthentication(Authentication):
    """
    An authentication mechanism that piggy-backs on Django sessions.

    This is useful when the API is talking to Javascript on the same site.
    Relies on the user being logged in through the standard Django login
    setup.

    Requires a valid CSRF token.
    """
    def is_authenticated(self, request, **kwargs):
        """
        Checks to make sure the user is logged in & has a Django session.
        """
        # Cargo-culted from Django 1.3/1.4's ``django/middleware/csrf.py``.
        # We can't just use what's there, since the return values will be
        # wrong.
        # We also can't risk accessing ``request.POST``, which will break with
        # the serialized bodies.
        if request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            return request.user.is_authenticated()
