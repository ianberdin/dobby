# coding: UTF-8
import datetime

from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=100, verbose_name=u"Название компании")
    phone = models.CharField(max_length=30, verbose_name=u"Номер телефона")
    address = models.CharField(max_length=100, verbose_name=u"Адрес")

    class Meta:
        verbose_name = u"Компания"
        verbose_name_plural = u"Компании"

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=100)
    x = models.IntegerField(default=100, verbose_name=u"X-координата")
    y = models.IntegerField(default=100, verbose_name=u"Y-координата")

    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"

    def __unicode__(self):
        return self.name


class Track(models.Model):
    city_from = models.ForeignKey(City, verbose_name=u"Из города", related_name="city_from")
    city_to = models.ForeignKey(City, verbose_name=u"В город", related_name="city_to")
    distance = models.IntegerField(verbose_name=u"Расстояние", help_text=u"В киллометрах", default=0)
    time = models.IntegerField(verbose_name=u"Длительность поездки", help_text=u"В часах", default=0)
    cost = models.IntegerField(verbose_name=u"Стоимость перевозки 1кг", help_text=u"В рублях", default=0)
    companies = models.ManyToManyField(Company, verbose_name=u"Компании", help_text=u"ВЫБЕРИТЕ ОДНУ КОМПАНИЮ!")

    class Meta:
        verbose_name = u"Маршрут"
        verbose_name_plural = u"Маршруты"

    def __unicode__(self):
        return self.get_track()

    def get_company_name(self):
        if self.companies.all():
            return self.companies.all()[0].name
        return None
    get_company_name.short_description = u'Название компании'

    def get_track(self):
        return "%s - %s" % (self.city_from.name, self.city_to.name)
    get_track.short_description = u'Маршрут'

    def get_id(self):
        return 'Маршрут №%s' % self.id
    get_id.short_description = u'Номер маршрута'


class Order(models.Model):
    ACTIVE = 'active'
    COMPLETE = 'complete'
    CANCEL = 'cancel'
    IN_PROGRESS = 'in_progress'
    STATUS_CHOICES = (
        (ACTIVE, 'Активная'),
        (COMPLETE, 'Завершено'),
        (CANCEL, 'Отменена'),
        (IN_PROGRESS, 'В процессе'),
    )

    track = models.ForeignKey(Track, verbose_name=u"Маршрут")
    message = models.CharField(u'Сообщение', max_length=255)
    status = models.CharField(u'Статус', max_length=70, choices=STATUS_CHOICES, default=ACTIVE)
    date_created = models.DateTimeField(u'Дата создания', auto_now_add=True, editable=False)
    date_updated = models.DateTimeField(u'Дата обновления', auto_now=True)

    class Meta:
        verbose_name = u"Заявка"
        verbose_name_plural = u"Заявки"
        ordering = ['-date_created']

    def __unicode__(self):
        return '%s %s' % (self.get_track(), self.get_company_name())

    def get_track(self):
        return self.track.get_track()
    get_track.short_description = u'Маршрут'

    def get_company_name(self):
        return self.track.get_company_name()
    get_company_name.short_description = u'Название компании'

    def get_created(self):
        return self.date_created.strftime("%d.%m.%y %H:%M")

    def get_id(self):
        return 'Заявка №%s' % self.id
    get_id.short_description = u'Номер заявки'
