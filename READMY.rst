Project tree:
---------------------

    dobby_project/
        dobby/
            dobby/
                settings/
                    base.py
                    local.py
                    production.py
                    dev.py
                    secret.example
                    secret.py
                urls.py
                wsgi.py
            core/
            media/
            static/
            templates
            fabfile.py
            manage.py
        .gitignore
        READMY.md
        requirements.txt

.. * base.py - основные настройки
.. * local.py - для локального развертывания
.. * production.py - для dobby.pro
.. * dev.py - для dev.dobby.pro

Проект запускаем с соответствующим сеттингом, например:
python manage.py runserver --settings=dobby.settings.production

Все доступы указываем в файле secret.py, в репозитории пример.
